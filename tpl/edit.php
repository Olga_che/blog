<?php
include_once("header.php");
?>
 <form method="post">
        <div class="col-md-9 blog-body">
       
            <div class="post">
                
                <h1><?=$header;?></h1>
                
                
                
                    <div class="form-group">
                        <label>Post title</label>
                        <input type="text" class="form-control" name="title" value = <?php $post['title']; ?>>
                    </div>
                    
                    <div class="form-group">
                        <label>Post summary</label>
                        <textarea class="form-control" name="summary" ><?php $post['summary']; ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <label>Post body</label>
                        <textarea rows="10" class="form-control" name="body"><?php $post['body']; ?></textarea>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary form-control" value="edit" />
                    </div>
               
                
                
                <hr />
            </div>
             
        </div>    
      </form>  
<?php include_once("bottom.php");
?>

<?php
require_once 'header.php';

?>
		
        <div class="col-md-9 blog-body">
            <form method="POST">
            <div class="post">
    
            <?php
            echo "<select name='time' class='btn btn-primary btn-sm'><option>All</option>";
            echo "<option>Year</option>";
            echo "<option>Days</option>";
            echo "<option>7 days</option>";
            echo "<option>Days</option></select>";
				foreach($qqq->posts as $post){
			?>           
            <div class="post">
                
                <h2 class="post-title"> <?php echo $post['title']; ?></h2>
                <h3 class="post-subtitle">
                      <?php echo $post['summary']; ?>  
                </h3>
                 <?php      ?>   
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by <?php  echo $post["date"] ?>
                    <a href='post.php?id=<?php echo $post['id']; ?>' class="btn btn-primary btn-sm pull-right">Read More</a>
                </p>
                <hr />
            </div>
            <?php } ?>
            
            <ul class="pagination pull-right" boundary-links="true">

                <?php if ($page > 1) {
                    $previous = $page - 1;
                    echo "<li><a href='?page=1' class='ng-binding'>First</a></li>";
                    echo "<li><a href='?page={$previous}' class='ng-binding'>Previous</a></li>";
                } else {
                    if ($pages !== 0) {
                        echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'>First</a></li>";
                        echo "<li class='ng-scope disabled'><a href='?page=' class='ng-binding'>Previous</a></li>";
                    }
                }
                 for ($i = $page - 2; $i < $page + 3; $i++) {
                    if ($i > 0 && $i <= $pages) {
                        if ($i == $page) {
                            echo "<li class='active'><a href='?page={$i}' class='ng-binding'>" . $i . "</a></li>";
                        } else {
                            echo "<li><a href='?page={$i}' class='ng-binding'>" . $i . "</a></li>";
                        }
                    }
                }
                if ($page < $pages) {
                    $next = $page + 1;
                    echo "<li><a href='?page=$next'>Next</a></li>";
                    echo "<li><a href='?page=$pages'>Last</a></li>";
                } else {
                    if ($pages !== 0) {
                        echo "<li class='ng-scope disabled'><a href='?page=$pages'>Next</a></li>";
                        echo "<li class='ng-scope disabled'><a href='?page=$pages'>Last</a></li>";
                    }
                }
                ?>
            </ul>
        </div>  
        </form>  
 <?php include_once("bottom.php");
?>

<?php
include_once('header.php');
?>
        <div class="col-md-9 blog-body">
        
            <div class="col-lg-6 col-lg-offset-3 ng-scope">
            
            
                <div class="panel panel-success" style="margin-top:20px;">
                    <div class="panel-heading">
                        <h2 style="margin:0;" class="ng-binding"><?=$header;?></h2>
                    </div>
                    <div class="panel-body">
                
                        
                            <form method="POST" action="" ng-submit="loginForm.submit()" novalidate="" name="loginFrm" class="ng-pristine ng-valid-email ng-invalid ng-invalid-required">
                                <?php if(isset($error))
                                        echo '<div class="alert alert-danger">Auth Error!</div>';
                                      if(isset($_SESSION["authkey"]))
                                        '<div class="alert alert-success">Auth Success!</div>';
                                ?>
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-email">Username</label>
                                    <input type="text" id="loginform-username" class="form-control ng-pristine ng-untouched ng-valid-email ng-invalid ng-invalid-required" name="username" ng-model="loginForm.email" ng-required="true" required="required">
                                </div>
                    
                                <div class="form-group required ">
                                    <label class="control-label ng-binding" for="loginform-password">Password</label>
                                    <input type="password" id="loginform-password" class="form-control ng-pristine ng-untouched ng-invalid ng-invalid-required" name="password" ng-model="loginForm.password" ng-required="true" required="required">
                                </div>
                    
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="remember-me" name="rememberMe" ng-model="loginForm.rememberMe" class="ng-pristine ng-untouched ng-valid"> Keep me logged in
                                    </label>
                                </div>
                    
                                <div class="form-group">
                                    <input type="submit" id="login-button" class="btn btn-primary form-control" value="Login">
                                </div>
                				<div>
                                    <input type="submit" class="btn" value="VK" click="https://oauth.vk.com/authorize?client_id=5100311&display=page&scope=email,photos,wall&display=page&redirect_uri=http://localhost/olga/blog/vkauth.php">
                                </div>
                            </form>
                
                    </div>
                </div>
            </div>
        </div>    
        
   <?php include_once("bottom.php");
?>
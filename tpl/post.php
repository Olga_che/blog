<?php include_once('header.php'); ?>
        <div class="col-md-9 blog-body">
        
            <div class="post">
                <h2 class="post-title"><?php echo $post['title']; ?></h2>
                <h3 class="post-subtitle">
                    <?=$post['body'];?>
					
                </h3>
                    
                <p class="post-meta"><span class="glyphicon glyphicon-time"></span> Posted by  <?=$post['date'];?>
					<?php if(isset($_SESSION['authkey'])){ ?>
					 <a href='delete.php?id=<?php echo $post['id']; ?>' class="btn btn-primary btn-sm pull-right">Delete</a>
					 <a href='edit.php?id=<?php echo $post['id']; ?>' class="btn btn-primary btn-sm pull-right">Edit</a> <?php } ?>
                </p>
				 
                
                <hr />
            </div>
            
        </div>    
        
   <?php include_once("bottom.php");
?>
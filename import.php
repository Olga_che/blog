<?php
$header = "Import VK";
$config = require_once ('config.php');

if (isset($_SESSION['access_token'])){
	$url = "https://api.vk.com/method/wall.get" ;
	$ch = curl_init($url);
	curl_setopt_array($ch, [
		CURLOPT_RETURNTRANSFER	 => true,
		CURLOPT_SSL_VERIFYPEER	 => false,
		CURLOPT_SSL_VERIFYHOST	 => false,
	CURLOPT_POST			 => true,
		CURLOPT_POSTFIELDS		 => [
			'owner_id'		 => $config['my_id'], 
			'count'			 => '20',
			'offset'		 => '0',
			'filter'		 => 'owner', 
			'access_token'	 => $_SESSION['access_token'],
		]
	]);

	if ($data = curl_exec($ch)){
		$data = json_decode($data, true);
	
			header('location: index.php');	
		}

		require_once ('tpl/import.php');
	}
}
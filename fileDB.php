<?php

abstract class Db {
    abstract function createPost($title, $summary, $body);
    abstract function updatePost($postId, $title, $summary, $body);
    abstract function getPosts($page);
   	abstract function postsCount();
    abstract function deletePost($postId);
}  

class FileDB extends Db{
 	private $pageSize;
 	private $fileParth;
	public $posts = [];
// 	private $post = [];
 	//private $fileLink = false;
 	const FORMAT = "Y-m-d H:i:s";
 		  function __construct($fileParth, $pageSize = 5){
 		  	$this->pageSize = $pageSize;
 		  	$this->fileParth = $fileParth;

 		  	if (!isset($this->fileParth)){
            	die("File not found");
        	}
 		  }

 		function createPost($title, $summary, $body){
 			$this->id = sha1(time() . mt_rand(0, 10000));
 			$this->post = [
 				
            	"title" => $title,
            	"summary" => $summary,
            	"body" => $body,
            	"date" => date(self::FORMAT),
            	"id" => $this->id,
            	

 			];

 		file_put_contents($this->fileParth, json_encode($this->post) . "\n" , FILE_APPEND);
        file_put_contents("db/" . $this->id . ".json", json_encode($this->post));
    	}	
	
	function updatePost($postId, $title, $summary, $body){ 
		$json_id = "db/" . $postId . ".json";
		$post["title"] = $title;
		$post["summary"] = $summary;
		$post["body"] = $body;
		$post["date"] = date(self::FORMAT);
		$post["id"] = $postId;	
		
		$post = json_encode($post);
		
		file_put_contents($json_id, $post);
		$json_id = json_decode($json_id, true);
			
		
		$read_file = file($this->fileParth);
	 	for($i=0; $i<=count($read_file)-1; $i++){
			
			if(isset($read_file[$i])){
				
				$b = $read_file[$i];
				$as = json_decode($b, true);
				
					if($as['id'] !== $_GET['id']){
						$posts[] = $as;
						
					}
					if($as['id'] == $_GET['id']){
						$posts[] = json_decode($post);
						
					}	
				}
		}
		file_put_contents($this->fileParth, "");
		
		foreach($posts as $post){
			
        	file_put_contents($this->fileParth, json_encode($post) . "\n", FILE_APPEND);
    	
		
		}
		
		
	}
	
	function postsCount(){
		//$read_file = file($this->fileParth);
		return count(file($this->fileParth))-1;
			
			
	}
    function deletePost($postId){
		
		$read_file = file($this->fileParth);
	 	for($i=0; $i<=count($read_file)-1; $i++){
			
			if(isset($read_file[$i])){
				
				$b = $read_file[$i];
				$as = json_decode($b, true);
				
					if($as['id'] !== $postId){
						$posts[] = $as;
					}	
				}
		}
		
		file_put_contents($this->fileParth, "");
		unlink("db/" . $postId . ".json");
		
		foreach($posts as $post){
			
        	file_put_contents($this->fileParth, json_encode($post) . "\n", FILE_APPEND);
    	
		
		}
				
	}
	function getPosts($page){
		$this->posts = [];
		$read_file = file($this->fileParth);
	 	 
     $p = ($page -1) * $this->pageSize;
	 $end = $p + $this->pageSize;
 
	 	for($i=$p; $i<=$end-1; $i++){
			if(isset($read_file[$i])){
				
				$b = $read_file[$i];
				$as = json_decode($b, true);
					if(!is_null($as)){
						$this->posts[] = $as;
					}	
				}
		}
		

	}
	
// 	function __get($num){
// 		$read_file = file($this->fileParth);

// 		$now =  new DateTime();

		
// 			for($j=0; $j<=$num; $j++){
// 				$days = new DateInterval("P1D");
// 				$days = $now->sub($days);


// 				for($i=0; $i<=count($read_file)-1; $i++){
			
// 					if(isset($read_file[$i]) ){
// 						$b = $read_file[$i];
// 						$as = json_decode($b, true);

// 						$s1 = substr($as['date'], 0, 10);
// 						if($s1 == $days->format('Y-m-d')){
// 							echo "<pre>";
// 							var_dump($as);
// 							echo "</pre>";
// 			}
// 		}

// }}	}
 	
function __get($num){
		$read_file = file($this->fileParth);

		$now =  new DateTime();

		
			for($j=0; $j<=$num; $j++){
				$days = new DateInterval("P1D");
				$day = $now->sub($days);


				for($i=0; $i<=count($read_file)-1; $i++){
			
					if(isset($read_file[$i]) ){
						$b = $read_file[$i];
						$as = json_decode($b, true);

						$d1 = strtotime($as['date']);
						$date2 = date("Y-m-d", $d1);
						var_dump($date2);
						// if($d1 == $day){
						// 	var_dump($as['date']);
						// }
			}
		}

}}	



//  	$date = "2013-06-17 19:00:00";

// $d1 = strtotime($date); // переводит из строки в дату

// $date2 = date("Y-m-d", $d1);
	












	
	function __toString(){
        return count(file($this->filename)); 
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}